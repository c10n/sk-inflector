package partsofspeech

import djsushi.properties.*
import djsushi.properties.Number

class Noun(val paradigm: Paradigm, val gender: Gender, val number: Number, val case: Case): IsPartOfSpeech {

    init {
        if (paradigm !in listOf(Paradigm.SUBSTANTIVE, Paradigm.ADJECTIVAL, Paradigm.FUSED, Paradigm.UNCOMPLETE))
            throw IllegalArgumentException("Nouns can't accept the $paradigm paradigm.")
    }

}