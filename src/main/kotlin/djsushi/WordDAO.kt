package djsushi

import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*
import java.util.function.Supplier
import java.util.stream.Stream
import kotlin.reflect.KProperty1

object WordDAO {

	private var lineCount: Long = 0
	lateinit var streamSupplier: Supplier<Stream<String>>

	// sort the files into files named AA,AB,AC,AD...ZX,ZY,ZZ


	/**
	 * Reads a certain line at [index] in the file at [path] and returns it.
	 * If the line index is out of bounds, returns null.
	 * If the file at [path] does not exist, throws [NoSuchFileException]
	 */
	fun readLine(path: String, index: Long): String {
		return try {
			if (!this::streamSupplier.isInitialized)
				streamSupplier = Supplier { Files.lines(Paths.get(path)) }
			streamSupplier.get().use { it.skip(index).findFirst().get() }
		} catch (e: NoSuchElementException) {
			throw IndexOutOfBoundsException("index: $index")
		}
	}

	fun getLineCount(path: String): Long {
		if (!this::streamSupplier.isInitialized)
			streamSupplier = Supplier { Files.lines(Paths.get(path)) }
		return streamSupplier.get().count()
	}

	/**
	 * Searches the files organized by [by] for the String [s] and returns a [List] of [Word]s
	 */
	fun find(s: String, by: KProperty1<Word, *>): List<Word> {

		// find the filename of the file containing the word
		val fileName = "${Sorter.decideFile(s)}.csv"
		val path = "${Sorter.resourcesPath}sortedWords/by${by.name.capitalize()}/$fileName"

		var found = false
		val output = mutableListOf<Word>()

		// read the words from the File at [path] and iterate over the list of Word objects
		for (word in File(path).readLines().map {
			val split = it.split("\t")
			Word(split[0], split[1], split[2])
		}) {
			// depending on the 'by' argument, choose which part of the word (base, inflected or meta)
			// we're searching for
			val searchedData = when (by) {
				Word::base -> word.base
				Word::inflected -> word.inflected
				Word::meta -> word.meta
				else -> throw IllegalArgumentException("'by' argument should be one of these three: \"base\", \"inflected\" or \"meta\"")
			}

			// this starts adding the subsequent Words into the output. It stops adding as soon as it finds a
			// non-matching word
			if (searchedData == s && !found) {
				found = true
				output.add(word)
			} else if (searchedData == s && found) {
				output.add(word)
			} else if (searchedData != s && found) {
				break
			}
		}

		return output
	}
}
