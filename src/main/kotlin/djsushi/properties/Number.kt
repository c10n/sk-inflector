package djsushi.properties

enum class Number(val abbr: Char) {
    SINGULAR('s'),
    PLURAL('p');

    companion object {
        fun get(abbr: Char, partOfSpeech: PartOfSpeech): Number {
            when (partOfSpeech) {
                PartOfSpeech.NOUN -> when (abbr) {
                    's' -> return SINGULAR
                    'p' -> return PLURAL
                }
            }

            throw IllegalArgumentException("Abbreviation '$abbr' isn't in the list.")
        }
    }
}