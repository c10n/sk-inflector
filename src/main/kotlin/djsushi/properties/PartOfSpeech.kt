package djsushi.properties

enum class PartOfSpeech {

    NOUN,
    ADJECTIVE,
    PRONOUN,
    NUMERAL,
    VERB,
    PARTICIPLE,
    ADVERB,
    PREPOSITION,
    CONJUNCTION,
    PARTICLE,
    INTERJECTION,
    REFLEXIVE_MORPHEME,
    CONDITIONAL_MORPHEME,
    ABBREVIATION,
    PUNCTUATION,
    UNDEFINABLE_PART_OF_SPEECH,
    NON_VERBAL_ELEMENT,
    FOREIGN_LANGUAGE_CITATION,
    NUMBER,

    // The proper name is actually an extension for nouns...
    // so TODO add functionality with proper names
    PROPER_NAME,

    // Incorrect spelling is also an extension but for any word...
    // TODO the database doesn't contain any :q references
    INCORRECT_SPELLING;

    companion object {
        fun get(abbr: Char): PartOfSpeech {
            when (abbr) {
                'S' -> return NOUN
                'A' -> return ADJECTIVE
                'P' -> return PRONOUN
                'N' -> return NUMERAL
                'V' -> return VERB
                'G' -> return PARTICIPLE
                'D' -> return ADVERB
                'E' -> return PREPOSITION
                'O' -> return CONJUNCTION
                'T' -> return PARTICLE
                'J' -> return INTERJECTION
                'R' -> return REFLEXIVE_MORPHEME
                'Y' -> return CONDITIONAL_MORPHEME
                'W' -> return ABBREVIATION
                'Z' -> return PUNCTUATION
                'Q' -> return UNDEFINABLE_PART_OF_SPEECH
                '#' -> return NON_VERBAL_ELEMENT
                '%' -> return FOREIGN_LANGUAGE_CITATION
                '0' -> return NUMBER
            }

            throw IllegalArgumentException("Abbreviation '$abbr' isn't in the list.")
        }
    }

}