package djsushi.properties

enum class Gender(val abbr: Char) {
    MASCULINE_ANIMATE('m'),
    MASCULINE_INANIMATE('i'),
    FEMININE('f'),
    NEUTRUM('n');

    companion object {
        fun get(abbr: Char, partOfSpeech: PartOfSpeech): Gender {
            when (partOfSpeech) {
                PartOfSpeech.NOUN -> when (abbr) {
                    'm' -> return MASCULINE_ANIMATE
                    'i' -> return MASCULINE_INANIMATE
                    'f' -> return FEMININE
                    'n' -> return NEUTRUM
                }
            }

            throw IllegalArgumentException("Abbreviation '$abbr' isn't in the list.")
        }
    }
}