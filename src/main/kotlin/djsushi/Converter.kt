package djsushi

import djsushi.properties.*
import djsushi.properties.Number

object Converter {

	fun translateMeta(word: Word): String {
		val meta = word.meta

		var translated = ""

		when (PartOfSpeech.get(meta[0])) {
			PartOfSpeech.NOUN -> {
				translated += "podstatné meno "
				translated += when (Paradigm.get(meta[1], PartOfSpeech.NOUN)) {
					Paradigm.SUBSTANTIVE -> "substantívnej paradigmy, "
					Paradigm.ADJECTIVAL -> "adjektívnej paradigmy "
					Paradigm.FUSED -> "zmiešanej paradigmy, "
					Paradigm.UNCOMPLETE -> "neúplnej paradigmy, "
					else -> ""
				}

				translated += when (Gender.get(meta[2], PartOfSpeech.NOUN)) {
					Gender.MASCULINE_ANIMATE -> "mužského životného rodu, "
					Gender.MASCULINE_INANIMATE -> "mužského neživotného rodu, "
					Gender.FEMININE -> "ženského rodu, "
					Gender.NEUTRUM -> "stredného rodu, "
				}

				translated += when (Number.get(meta[3], PartOfSpeech.NOUN)) {
					Number.SINGULAR -> "jednotného čísla "
					Number.PLURAL -> "množného čísla "
				}

				translated += when (Case.get(meta[4], PartOfSpeech.NOUN)) {
					Case.NOMINATIVE -> "v nominatíve"
					Case.GENITIVE -> "v genitíve"
					Case.DATIVE -> "v datíve"
					Case.ACCUSATIVE -> "v akuzatíve"
					Case.VOCATIVE -> "vo vokatíve"
					Case.LOCATIVE -> "v lokáli"
					Case.INSTRUMENTAL -> "v inštrumentáli"
				}
			}
			PartOfSpeech.ADJECTIVE -> TODO()
			PartOfSpeech.PRONOUN -> TODO()
			PartOfSpeech.NUMERAL -> TODO()
			PartOfSpeech.VERB -> TODO()
			PartOfSpeech.PARTICIPLE -> TODO()
			PartOfSpeech.ADVERB -> TODO()
			PartOfSpeech.PREPOSITION -> TODO()
			PartOfSpeech.CONJUNCTION -> TODO()
			PartOfSpeech.PARTICLE -> TODO()
			PartOfSpeech.INTERJECTION -> TODO()
			PartOfSpeech.REFLEXIVE_MORPHEME -> TODO()
			PartOfSpeech.CONDITIONAL_MORPHEME -> TODO()
			PartOfSpeech.ABBREVIATION -> TODO()
			PartOfSpeech.PUNCTUATION -> TODO()
			PartOfSpeech.UNDEFINABLE_PART_OF_SPEECH -> TODO()
			PartOfSpeech.NON_VERBAL_ELEMENT -> TODO()
			PartOfSpeech.FOREIGN_LANGUAGE_CITATION -> TODO()
			PartOfSpeech.NUMBER -> TODO()
			PartOfSpeech.PROPER_NAME -> TODO()
			PartOfSpeech.INCORRECT_SPELLING -> TODO()
		}

		return translated
	}

	fun parseMeta() {

	}

}