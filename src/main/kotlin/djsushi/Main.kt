package djsushi

import djsushi.properties.PartOfSpeech

const val byBasePath =
	"/home/djsushi/projects/IJ/sk-inflector/src/main/resources/sortedWords/byBase/"
const val byInflectedPath =
	"/home/djsushi/projects/IJ/sk-inflector/src/main/resources/sortedWords/byInflected/"
const val byMetaPath =
	"/home/djsushi/projects/IJ/sk-inflector/src/main/resources/sortedWords/byMeta/"



// This is just an execution and testing environment, the future library code will be in other files

/*
--- Example ---

mačka	mačka	SSfs1
mačka	mačky	SSfs2
mačka	mačke	SSfs3
mačka	mačku	SSfs4
mačka	mačka	SSfs5
mačka	mačke	SSfs6
mačka	mačkou	SSfs7
mačka	mačky	SSfp1
mačka	mačiek	SSfp2
mačka	mačkám	SSfp3
mačka	mačky	SSfp4
mačka	mačky	SSfp5
mačka	mačkách	SSfp6
mačka	mačkami	SSfp7
 */

fun main() {

	// set the path to the resources folder
	// it has to have a slash at the end... TODO add automatically a slash when there is none at the end
	Sorter.resourcesPath = "/home/djsushi/projects/IJ/sk-inflector/src/main/resources/"

	// set the name of the file with the main dictionary
	Sorter.sourceFileName = "ma-2015-02-05.txt"

	WordDAO.find("test", Word::inflected).forEach {
		println(it)
		println(Converter.translateMeta(it))
	}

}

fun sortEverything() {
	// clear the files (don't remove them, just remove the contents/make them empty)
	Sorter.clearFiles(Word::base)
	Sorter.clearFiles(Word::inflected)

	// extract the words from the main dictionary and sort them into files
	Sorter.sort(Word::base)
	Sorter.sort(Word::inflected)
}
